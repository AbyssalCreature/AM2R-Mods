# AM2R-Mods
A repository listing available Mods for AM2R

> **NEITHER I, NOR THE AM2R-COMMUNITY-DEVELOPERS ARE RESPONSIBLE FOR ANY DAMAGE THESE MODS DO TO YOUR PC, USE AT YOUR OWN RISK!!!**   


Table of Contents:
<details>
<summary>[Mods for Speedrunning](#mods-for-speedrunning)</summary>

- [Practice Mod](#practice-mod-by-nommiin)
- [Speedrun Mod](#speedrun-mod-by-miepee-and-metroid3d)
- [Old Speedrun Mod](#old-speedrun-mod-by-metroid3d)
- [Unofficial 1.1.3](#unofficial-113-by-miepee-lojical-banjo)

</details>
<details>
<summary>[Mods for 1.1](#mods-for-11)</summary>

- [AM2R FATE](#am2r-fate-by-saber-zero)
</details>
<details>
<summary>[Mods for Community Updates](#mods-for-community-updates)</summary>

- [AM2R Multitroid](#am2r-multitroid-by-milesthenerd)
- [AM2R The Horde](#am2r-the-horde-by-jesright)
- [Always Fusion-Mode](#always-fusion-mode-by-miepee)
- [SeedViewer](#seedviewer-by-miepee)
- [AM2R Fusion Sound Pack](#am2r-fusion-sound-pack-by-snakelancer)
- [Another Memetroid 2 Remake](#another-memetroid-2-remake-by-the-metroid)
- [AM2R Area Randomizer](#am2r-area-randomiser-by-dodobirb)
- [AM2R Turbo Edition](#am2r-turbo-edition-by-dodobirb)
- [AM2R Hellrun](#am2r-hellrun-by-dodobirb)
</details>
<details>
<summary>[Mods for AM2R Demos](#mods-for-am2r-demos-by-doctorm64)</summary>

- [Demo 1.0](#am2r-demo-10)
- [Demo 1.1](#am2r-demo-11)
- [Demo 1.1A](#am2r-demo-11a)
- [Demo 1.2](#am2r-demo-112)
- [Demo 1.2.1](#am2r-demo-121)
- [Demo 1.3](#am2r-demo-13)
- [Demo 1.3.1](#am2r-demo-131)
- [Demo 1.3.2](#am2r-demo-132)
- [Demo 1.3.3](#am2r-demo-133)
- [Demo 1.3.4](#am2r-demo-134)
- [Demo 1.4](#am2r-demo-14)
- [Demo 1.4.1](#am2r-demo-141)
</details>
<details>
<summary>[Mods for archived, older AM2R versions](#mods-for-older-am2r-versions)</summary>

- [AM2R 1.0](#am2r-10-by-doctorm64)
- [AM2R 1.1](#am2r-11-by-doctorm64)
- [AM2R 1.2.1](#am2r-121)
- [AM2R 1.2.2](#am2r-122)
- [AM2R 1.2.3](#am2r-123)
- [AM2R 1.2.4](#am2r-124)
- [AM2R 1.2.5](#am2r-125)
- [AM2R 1.2.6](#am2r-126)
- [AM2R 1.2.7](#am2r-127)
- [AM2R 1.2.8](#am2r-128)
- [AM2R 1.2.9](#am2r-129)
- [AM2R 1.2.9A](#am2r-129a)
- [AM2R 1.2.10](#am2r-1210)
- [AM2R 1.3](#am2r-13)
- [AM2R 1.3.1](#am2r-131)
- [AM2R 1.3.2](#am2r-132)
- [AM2R 1.3.3](#am2r-133)
- [AM2R 1.4](#am2r-14)
- [AM2R 1.4.1](#am2r-141)
- [AM2R 1.4.2](#am2r-142)
- [AM2R 1.4.3](#am2r-143)
- [AM2R 1.5](#am2r-15)
- [AM2R 1.5.1](#am2r-151)
- [AM2R 1.5.2](#am2r-152)
</details>

# Mods for Speedrunning

## Practice Mod by Nommiin
A mod based on YAL's reconstruction, used for speedrunning practice. Has savestates, in order to easily practice certain Areas or splits.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_Practice_Mod.zip?inline=false).  

## Speedrun Mod by Miepee and Metroid3D
A modified of AM2R 1.1 used for speedrunning practice. Has savestates as well as a lot of debug functionality. An extensive list of features can be found in the Profile Notes.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/Speedrun_Mod_AM2R_1.1.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/Speedrun_Mod_AM2R_1.1_lin.zip?inline=false).

## Old Speedrun Mod by Metroid3D
A modified version of AM2R 1.1.2 used for speedrunning practice. Has savestates, all sorts of debug information, quick room warping and much more.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_OldM3DSpeedrun_Mod.zip?inline=false).  

## Unofficial 1.1.3 by Miepee, Lojical & Banjo
This is a recreation of Banjo's 1.1.2 mod, designed to be as close to 1.1 from a bytecode perspective. The original 1.1.2 had some code evaluation differences. This mod should circumvent these issues by not using YellowAfterlife's AM2R reconstruction.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1_1_3.zip?inline=false).

---

# Mods for 1.1

## AM2R FATE by Saber Zero
A modified version of AM2R 1.1 intended to make you feel overpowered. Hyper Beam is avilable from the start, as well as instantly shinesparking with the `AimLock`-button. This is preconfigured for *only* the third save file.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FATE%20AM2R.zip?inline=false).

---

# Mods for Community Updates

## AM2R Multitroid by milesthenerd
A modified version of AM2R 1.5.2 that allows you to play co-op with up to 8 people online. More information can be found on milesthenerd's [Repository](https://github.com/lassiterm/AM2R-Multitroid/).  
Download links (Windowns, Linux, Android) can be found [here](https://github.com/lassiterm/AM2R-Multitroid/releases/latest).

## AM2R The Horde by JesRight
What if Samus was late to her mission on SR-388? Find out in this ruthlessly challenging mod of AM2R. The hornoads are waiting.  
Built upon Multitroid 1.3, the online multiplayer mod by milesthenerd. 
Download links (Win, Lin, Android, Mac) can be found here: <https://github.com/Hornoads/AM2R-The-Horde-Multitroid/releases>

## SeedViewer by Miepee
This is a Mod that upon loading a save files puts you into an interactive map where you can see where all the items are located. Consider this like a Spoiler log. Based on 1.5.2, might not be compatible with older or newer seed generators.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/SeedViewerRelease.zip).

## Always Fusion-Mode by Miepee  
This mod will have the fusion mode features (aka fusion suit, X and CoreX) always enabled after creating a new save, regardless of difficulty. Old saves are not affected. This means, that you can enjoy the features of Fusion Mode, without its hard difficulty.  
Android builds are included.  
Download for 1.5.2: [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways_win.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways_lin.zip?inline=false).  
Download for 1.5.4: [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways154_win.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/FusionSuitAlways154_lin.zip?inline=false).

## AM2R Fusion Sound Pack by SnakeLancer
A modified version of AM2R 1.5.2 that replaces all of the songs with Fusion related ones. Some are just taken straight out of Fusion, some are remixes.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Fusion%20Sound%20Pack.zip?inline=false).

## Another Memetroid 2 Remake by The Metroid
A modified version of AM2R 1.5.2 that introduces memes in text as well as sound. Has some crude humor.  
Android downloads are included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/Another_Memetroid_2_Remake_V3.4.3.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/Another_Memetroid_2_Remake_lin_V3.4.3.zip?inline=false).

## AM2R Area Randomiser by DodoBirb
A mod for AM2R 1.5.5 that randomises certain door transitions spread across the map. Based on the Super Metroid Area Rando.  
 Download links (Windows, Linux, Android) can be found [here](https://github.com/DodoBirby/AM2R-Area-Rando/releases/latest)

## AM2R Turbo Edition by DodoBirb
A mod for AM2R 1.5.5 that adds several new speedbooster techniques. Cancel shinesparks into run mid-air with aimlock, keep your speed without mockballing, and store charge by spinjumping into a wall.  
 Download link for Windows [here](https://github.com/DodoBirby/AM2R-Turbo-Edition/releases/latest)

## AM2R Hellrun by DodoBirb
AM2R 1.5.5 mod where all rooms outside of save points will deal damage to you. Move quickly to stay alive!  
 Download link for Windows [here](https://github.com/DodoBirby/AM2R-Hellrun-Mod/releases/latest)

---

# Mods for AM2R Demos by DoctorM64
These are mods, so you can launch the respective demos straight from the Launcher without any issues.  

## AM2R Demo 1.0
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.0.zip?inline=false).

## AM2R Demo 1.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.1.zip?inline=false).

## AM2R Demo 1.1A
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.1a.zip?inline=false).

## AM2R Demo 1.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.2.zip?inline=false).

## AM2R Demo 1.2.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.21.zip?inline=false).

## AM2R Demo 1.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.3.zip?inline=false).

## AM2R Demo 1.3.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.31.zip?inline=false).

## AM2R Demo 1.3.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.32.zip?inline=false).

## AM2R Demo 1.3.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.33.zip?inline=false).

## AM2R Demo 1.3.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.34.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.34Linux.zip?inline=false).

## AM2R Demo 1.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.4.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.4Linux.zip?inline=false).

## AM2R Demo 1.4.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.41.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%20Demo%20v1.41Linux.zip?inline=false).

---

# Mods for older AM2R versions

## AM2R 1.0 by DoctorM64
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.0.zip?inline=false).

## AM2R 1.1 by DoctorM64
This just is an almost empty mod, so that you can launch AM2R 1.1 from the Launcher without any issues.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_by_DoctorM64.zip) and unnofficial [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R_1.1_by_DoctorM64Linux.zip).

## The following Mods are Community Updates from the Community Developers

## AM2R 1.2.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.1.zip?inline=false).

## AM2R 1.2.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.2.zip?inline=false).

## AM2R 1.2.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.3.zip?inline=false).

## AM2R 1.2.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.4.zip?inline=false).

## AM2R 1.2.5
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.5.zip?inline=false).

## AM2R 1.2.6
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.6.zip?inline=false).

## AM2R 1.2.7
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.7.zip?inline=false).

## AM2R 1.2.8
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.8.zip?inline=false).

## AM2R 1.2.9
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.9.zip?inline=false).

## AM2R 1.2.9A
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.9A.zip?inline=false).

## AM2R 1.2.10
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.2.10.zip?inline=false).

## AM2R 1.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.zip?inline=false).

## AM2R 1.3.1
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.1.zip?inline=false).

## AM2R 1.3.2
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.2.zip?inline=false).

## AM2R 1.3.3
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.3.3.zip?inline=false).

## AM2R 1.4
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.zip?inline=false).

## AM2R 1.4.1
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.1.zip?inline=false).

## AM2R 1.4.2
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.2.zip?inline=false).

## AM2R 1.4.3
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.4.3.zip?inline=false).

## AM2R 1.5
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5Lin.zip?inline=false).

## AM2R 1.5.1
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.1.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.1Lin.zip?inline=false).

## AM2R 1.5.2
This has the official Android releases included.  
Download for [Windows](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.2.zip?inline=false) and [Linux](https://gitlab.com/Miepee/AM2R-Mods/-/raw/main/mods/AM2R%201.5.2Lin.zip?inline=false).
